require("dotenv").config();
const express = require("express");
const path = require("path");
const app = express();
const morgan = require("morgan");
const api = require("./api");
const port = 3021;
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");

if (process.env.MODE == "production") {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({
        mongoUrl: `mongodb://2217021:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/2217021?authSource=admin`,
      }),
    })
  );
} else {
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: false,
      store: MongoStore.create({ mongoUrl: "mongodb://localhost/goose" }),
    })
  );
}
app.use(passport.initialize());
app.use(passport.session());
app.use(
  morgan("dev"),
  bodyParser.json({ extended: false }),
  bodyParser.urlencoded({ extended: false })
);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect("/");
}

const serveTablePage = (req, res) => {
  res.sendFile(path.join(__dirname, "./public/table.html"));
};
app.get("/table", ensureAuthenticated, serveTablePage);
app.get("/table.html", ensureAuthenticated, serveTablePage);
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./public/index.html"));
});

app.use(express.static("public"), express.static("dist"));

app.use("/api", api);
async function main() {
  if (process.env.MODE == "production") {
    await mongoose.connect(`mongodb://192.168.171.67:27017/nic`, {
      useNewUrlParser: true,
      authSource: "admin",
      user: "nic",
      pass: process.env.MONGO_PASSWORD,
    });
  } else {
    await mongoose.connect("mongodb://localhost:27017/goose");
  }

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
}
main().catch((err) => console.error(err));
